export default function VideoIdContainer({ video }) {
  let channelThumbnail = video.channelData.thumbnails[0];
  return (
    <div className="page-container">
      <iframe
        width="420"
        height="315"
        src={`https://www.youtube.com/embed/${video.videoData.videoId}?autoplay=1`}
        frameBorder="0"
        allow='autoplay'
        allowFullScreen
      ></iframe>
      <h2>{video.videoData.title}</h2>
      <div>
        <div className="thumbnail-container"></div>
        <div className="video-item">
          <div>Description : {video.videoData.description}</div>
          <div>Likes: {video.videoLikesDislikes.likes}</div>
          <div>Dislikes: {video.videoLikesDislikes.dislikes}</div>
          <div>Views: {video.videoData.viewCount}</div>

          <h3> Channel Data </h3>
          <div>
            <img
              src={channelThumbnail.url}
              height={channelThumbnail.height}
              width={channelThumbnail.width}
            />
          </div>
          <div>Channel Name: {video.videoData.channelName}</div>
          <div>Channel Subscribers: {video.channelData.subscribers}</div>
          <div>Channel Description: {video.channelData.description}</div>
        </div>
      </div>
      <style>
        {`
      .video-item {
        padding : 10px 0; 
      }
      .page-container {
        padding : 20px;
      }
      `}
      </style>
    </div>
  );
}

export async function getServerSideProps(context) {
  let { query } = context;
  let { videoId } = query;
  let BASE_URL = process.env.API_BASE_URL;
  let res = await fetch(`${BASE_URL}/video/${videoId}`);
  const video = await res.json();

  return {
    props: {
      video: video.data,
    },
  };
}
