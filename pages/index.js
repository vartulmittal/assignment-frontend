import Link from "next/link";

export default function Home({ videos }) {
  return (
    <div className="page-container">
      <h2>Popular Videos</h2>
      <div>
        {videos.map((video) => {
          let thumbnail = video.videoThumbnails[3];
          return (
            <Link href={video.videoId} key={video.videoId}>
              <a href={video.videoId}>
                <div className="video-container">
                  <div className="thumbnail-container">
                    <img
                      src={thumbnail.url}
                      height={thumbnail.with}
                      width={thumbnail.height}
                    />
                  </div>
                  <div className="video-item">
                    <h3>{video.title}</h3>
                    <h5>{video.description}</h5>
                    <div>Views: {video.viewCount}</div>
                    <div>Author: {video.channelName}</div>
                  </div>
                </div>
              </a>
            </Link>
          );
        })}
      </div>
      <style>
        {`
      .video-item {
        margin-left : 20px;
      }

      .video-container {
        display : flex;
        padding : 10px 0; 

      }
      .page-container {
        padding : 20px;
      }
      `}
      </style>
    </div>
  );
}

export async function getServerSideProps(context) {
  let BASE_URL = process.env.API_BASE_URL;
  let res = await fetch(`${BASE_URL}/videos`);
  const videos = await res.json();

  return {
    props: {
      videos: videos.data,
    },
  };
}
